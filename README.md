# Лабораторные работы по предмету ТРПО

## Лабораторная работа #1

### Определить основные процессы работы любого предприятия: определить бизнес-процессы и промоделировать их на языке IDEF0, дополнить IDEF3 & DFD. Это фактически фаза обследования будущего обьекта для автоматизации. 

Сперва рассмотрим данные методологии

#### IDEF0

Функциональная модель IDEF0 представляет собой набор блоков, каждый из которых представляет собой «черный ящик» со входами и выходами, управлением и механизмами, которые детализируются (декомпозируются) до необходимого уровня. Наиболее важная функция расположена в верхнем левом углу. А соединяются функции между собой при помощи стрелок и описаний функциональных блоков. При этом каждый вид стрелки или активности имеет собственное значение. Данная модель позволяет описать все основные виды процессов, как административные, так и организационные.

#### IDEF3

IDEF3 — методология моделирования и стандарт документирования процессов, происходящих в системе. Метод документирования технологических процессов представляет собой механизм документирования и сбора информации о процессах. IDEF3 показывает причинно-следственные связи между ситуациями и событиями в понятной эксперту форме, используя структурный метод выражения знаний о том, как функционирует система, процесс или предприятие.

#### DFD

DFD – это нотация, предназначенная для моделирования информационный систем с точки зрения хранения, обработки и передачи данных.

Для лабораторной работы было выбрано предприятие интенет-магазина, включающего в себя всё богатство бизнес-процессов, начиная от закупки продуктов у вендоров и заканчивая продажей конечным потребителям.

Ниже представлена общая IDEF0 схема предприятия
![lab1](/img/lab1_main.jpg "Общая схема предприятия")

Далее рассмотрим более подробную схему в виду IDEF0
![lab1](/img/lab1_client_flow.jpg "Client flow")


Далее рассмотрим процесс подготовки печатных отчетных форм при процессе продажи товара конечному клиенту. Схема в формате IDEF3
![lab1](/img/lab1_docs_flow.jpg "Documents flow")

Далее опишем нотацию автоматизации продаж в формате DFD. Сперва рассмотрим верхний уровень, избегая декомпозиции:
![lab1](/img/lab1_sales_dfd.jpg "Sales automation")

Далее проведем декомпозицию основного элемента нашей диаграммы:
![lab1](/img/lab1_sales_dfd_decompose.jpg "Sales automation")

Описав данные диаграммы мы выполнили требования первой ЛР

## Лабораторная работа #2

### Разработать требования к изделию = это не что иное как ТЗ

#### Общие сведения

Данный документ является техническим заданием на разработку Интернет магазина, который специализируется на продаже электроники

#### Язык

Английский (одноязычный сайт)

#### Структура интернет-магазина

На главной странице, которая будет видна конечному пользователю должно быть следующее:

1) Каталог товаров. Каталог из товаров, предлагаемых в данный момент интернет-магазином  
2) Страница заказа. При заказе товара пользователь должен видень конечную страницу для оформления заказа, где будет содержаться информация о приобретаемом товаре, количестве и конечной цене.  
3) Страница контактов. На странице контактов будут указаны контактные данные службы поддержки интернет-магазинов и какая-либо дополнительная информация, которая может понадобиться клиенту  

Также введем понятие портал. Портал - это та часть сайта, которая видна конечному пользователю. Помимо портала ещё есть веб-интерфейс, к которому имеют доступ лишь сотрудники интернет-магазина и где, в свою очередь, ведутся все бизнес-процессы, начиная от закупки у вендоров и заканчивая выставлением счетов и печатью чеков. Веб-интерфейс должен включать в себя следующие компоненты:  
1) Модуль Purchase. Модуль, где будут описаны все закупки, проводимые интернет-магазином. Касательно нашего предприятия, это будет закупка товаров, которые в дальнейшем будут выставлены на продажу.  
2) Модуль Sales. Модуль, где будут выписываться предварительные счета, далее оформляться заказы на продажу и на их основе создаваться инвойсы. Необходимо предусмотреть возможность печатать печатные формы для последующей отсылки клиенту, а также предусмотреть общение с потенциальным клиентом посредством электронной почты. Также необходимо аналитическая функциональност, которая позволила бы проводить аналитику работы команды продаж и находить способы улучшить эту работу.  
3) Модуль Accounting. Необходимо разработать модуль бухгалтерского учета, где будут храниться все инвойсы, банковские счета, составляться банковские выписки и идти учет кредиторской/дебиторской задолженностей, управление счетами и общее управление деньгами компании.  
4) Модуль HelpDesk. Данный модуль необходим для обработки клиентских заявок на тех.поддержку и своевременное решение возникающих проблем.  

По возможности внедрить POS (Point of Sale) модуль для возможности открыть оффлайн-точку для магазина и последующего уменьшения расходов на внедрение POS программного обеспечения.

#### Особенности функциональности

1) Система скидок для конечного пользователя. Необходимо предусмотреть возможность внедрения программы лояльности и последующего применения скидок при покупках лояльным клиентам.  
2) Система логистики. Предусмотреть и настроить функциональность связанную с хранением товара на складах и последующей дсотавкой этих товарах конечному пользователю.  
3) Уведомления по почте. Все действия, связанные с клиентом должны нотифицироваться клиенту на почту. Это значит, что если товар доставлен или отгружен со склада, то клиент должен получать соответствующую нотификацию от магазина.  
4) Учет всех taxes. В зависимости от страны, должны просчитываться налоги, также должна быть валидация vat id клиентов, что, в свою очередь, позволит правильно посчитать все налоги.  
5) Возможность составлять и печатать предварительные счета, заказы на продажу, накладные, инвойсы и любые печатные формы. Сделать возможность кастомизировать данные формы.  

## Лабораторная работа #3

### Описать в табличном и текстовом спец виде USE CASE (это не UML) для реализации требований. Начальная формализация.

# Условие: Описать в табличном и текстовом спец виде USE CASE (это не UML) для реализации требований. Начальная формализация.

**USE CASES:**

***Табличные***:

|      USE CASE 1        | Авторизация пользователя в системе |
| :--------------------: | - |
| **Цель**               | Проверить поведение системы при попытке авторизации с некорректным паролем |
| **Сценарий**           | Главный экран -> Log In -> заполнить поля -> кликнуть 'Log In'
| **Результат**          | Клиент получает ошибку, в которой говорится о том, что попытка авторизации провалена из-за ввода неправильного пароля |
 

|      USE CASE 2        | Попытка изменения системных данных обычным пользователем |
| :--------------------: | - |
| **Цель**               | Проверить устойчивость системы на предмет устойчивости к некорректному поведению пользователя и наличии защиты от взлома и изменения системных параметров рядовым пользователем |
| **Сценарий**           | Авторизоваться как обычный пользователь -> Попытаться зайти в Settings -> Users -> открыть пользователя и изменить его права
| **Результат**          | Модуль Settings не будет показан в списке доступных, потому что у рядового пользователя нет таких прав, а при попытке перейти в модуль по ссылке, пользователь будет перенаправлен в главное меню |


|      USE CASE 3        | Создание Sale Order менеджером по продажам |
| :--------------------: | - |
| **Цель**               | Проверить корректность создания sale order из-под пользователя менеджера по продажам, а также проверить правильность заполнения всех полей |
| **Сценарий**           | Авторизоваться в системе как менеджер по продажам -> перейти в модуль Sales -> создать Sale Order
| **Результат**          | Был создан Sale Order. В созданном заказе были корректно подставлены как клиентские поля, так и данные в нашей компании, что, в свою очередь, позволит быстро и удобно проводить сделки с клиентом в будущем |


|      USE CASE 4        | Проверка отправки про-формы клиенту |
| :--------------------: | - |
| **Цель**               | Проверить корректность механизма отправки про-формы посредством внутреннего механизма работы с сообщениями и почтой. |
| **Сценарий**           | Авторизоваться как продажник -> перейти в Sales -> открыть не подтвержденный Sale Order -> кликнуть 'Send PRO-FORMA Invoice' -> убедиться в правильности реквизитов -> кликнуть 'Send'
| **Результат**          | Справа в списке сообщений будет выведено сообщение, в котором можно увидеть текст сообщение а также саму про-форму в виде аттача и конечному клиенту на его почтовый адрес будет отправлено письмо с аттачем. |


|      USE CASE 5        | Попытка отправить товар клиенту со склада, где нет необходимого количества данного товара |
| :--------------------: | - |
| **Цель**               | Проверить механизм работы со складами. Убедиться в том, что мы не можем отправить конечному клиенту несуществующий товар. |
| **Сценарий**           | Авторизоваться как пользователь модуля логистики -> открыть заказ на доставку, который соответствует заданным условиям -> попытаться отправить товар клиенту
| **Результат**          | Товар не отправится, заказ не подтвердится. Пользователь системы получит ошибку в виде всплывающего окна, сообщающего о том, что невозможно отправить товар со склада, потому что его там нет |


|      USE CASE 6        | Попытка обнулить пароль пользователю при ненастроенном почтовом сервере |
| :--------------------: | - |
| **Цель**               | Проверка механизме генерации ссылок на форму восстановления пароля конкетного пользователя. |
| **Сценарий**           | Авторизоваться как администратор системы. Убедиться в том, что почтовые сервера не настроены -> Settings -> Users -> выбрать пользователя -> кликнуть Actions -> выбрать Reset Password -> отправить ссылку пользователю -> попросить пользователя перейти по ссылке
| **Результат**          | Пользователь, перейдя по ссылке, успешно восстаановит пароль и продолжит работу с системой |


|      USE CASE 7        | Попытка создать товар для продажи, не имея соответствующего разрешения в системе |
| :--------------------: | - |
| **Цель**               | Проверить систему разрешений и взаимодействие с системой производства |
| **Сценарий**           | Авторизоваться как пользователь, у которого нет доступа к модулю производства, но есть к модулю продаж -> открыть Sales -> попытаться создать новый заказ на продажу и в выпадающем списке найти кнопку создания нового продукта
| **Результат**          | Кнопки не будет, потому что пользователь не имеет соответствующих разрешений. Работа системы корректна |

***Текстовые***:  
1. Просмотр доступных банковских счетов для заказа на продажу - Кликнуть на поле 'Bank Account' в неподтвержденном заказе не продажу. Система выведет список из доступных банковских счетов для получения платежа.  
2. Проверка модуля коммуникации - В модуле Discuss в общем канале через @ упомянуть другого пользователя. Второй пользователь получит push уведомление в браузере и в правом верхнем углу приложения в списке уведомлений будет +1 уведомление.  
3. Проверка возможности отправить товар со склада на свалку - в неподтвержденном заказе наа доставку конечный пункт изменить на Scrap. Товар отправится в утилизацию.  
4. Проверка указать разные адреса для выставления счета и доставки - указать отличный адрес для оплаты товара. Автоматически будут высчитаны адреса.  
5. Проверка пересчета суммы инвойса в зависимости от выбранного прайслиста - поменять прайслист в неподтвержденном инвойсе. Будут пересчитаны все суммы в заказе.  

## Лабораторная работа #4

### На языке UML спроектировать ПО:
1. промоделировать реализацию use case с помощью S.D.;
2. перейти и выполнить моделирование с помощью диаграмм классов получить код для реализации (основа - см. диаграммы выше);
3. построить диарамму компонент из диаграмм классов;

**Выполненное задание:**
Sequence Diagram

Use case: Авторизация пользователя в системе

![](/lab4/sign_up_failed.jpg)

Use case: Проверка отправки про-формы клиенту

![](/lab4/pro_forma.jpg)

Component Diagram:
![](/lab4/structure.jpg)


## Лабораторная работа №4.2 ##

### На языке IDEF1X спроектировать БД, сгенировать DDL, построить БД, которая будет применяться в п. 4.1. ###

#### Выполненное задание: ####

IDEF1X

![](/lab4/idef1x.jpg)


DDL

Users

``` SQL

CREATE TABLE [dbo].[Users] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [FullName]        NVARCHAR (MAX) NULL,
    [Email]       NVARCHAR (MAX) NULL,
    [CompanyName] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED ([Id] ASC)
);

```

Tickets

``` SQL

CREATE TABLE [dbo].[Orders] (
    [Id]            INT IDENTITY (1, 1) NOT NULL,
    [TotalAmount]    INT NOT NULL,
    [UserId]        INT NULL,
    [InvoiceId] INT NULL,
    [TransferId]     INT NULL,
    CONSTRAINT [PK_Orders] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Orders_Invoice_InvoiceId] FOREIGN KEY ([InvoiceId]) REFERENCES [dbo].[Inovice] ([Id]),
    CONSTRAINT [FK_Orders_Transfers_TransferId] FOREIGN KEY ([TransferId]) REFERENCES [dbo].[Transfers] ([Id]),
    CONSTRAINT [FK_Transfers_Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([Id])
);

```

Performance

``` SQL

CREATE TABLE [dbo].[Invoice] (
    [Id]        INT            IDENTITY (1, 1) NOT NULL,
    [Number]      INT NOT NULL,
    [Product]  NVARCHAR (MAX) NULL,
    [Quantity]     INT NOT NULL,
    [TransferId] INT            NULL,
    CONSTRAINT [PK_Invoice] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Performance_Transfers_TransferId] FOREIGN KEY ([TransferId]) REFERENCES [dbo].[Transfers] ([Id])
);

```

Theatres

``` SQL

CREATE TABLE [dbo].[Transfers] (
    [Id]      INT            IDENTITY (1, 1) NOT NULL,
    [Destination]    NVARCHAR (MAX) NULL,
    [Product] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Transfers] PRIMARY KEY CLUSTERED ([Id] ASC)
);

```

## Лабораторная работа №5 ##

### Выполнить тестирование по любому критерию  ###

**Выполненное задание:**

Tests

```
class TestSaleOrder(TestCommonSaleNoChart):

    @classmethod
    def setUpClass(cls):
        super(TestSaleOrder, cls).setUpClass()

        SaleOrder = cls.env['sale.order'].with_context(tracking_disable=True)

        # set up users
        cls.setUpUsers()
        group_salemanager = cls.env.ref('sales_team.group_sale_manager')
        group_salesman = cls.env.ref('sales_team.group_sale_salesman')
        group_employee = cls.env.ref('base.group_user')
        cls.user_manager.write({'groups_id': [(6, 0, [group_salemanager.id, group_employee.id])]})
        cls.user_employee.write({'groups_id': [(6, 0, [group_salesman.id, group_employee.id])]})

        # set up accounts and products and journals
        cls.setUpAdditionalAccounts()
        cls.setUpClassicProducts()
        cls.setUpAccountJournal()

        # create a generic Sale Order with all classical products and empty pricelist
        cls.sale_order = SaleOrder.create({
            'partner_id': cls.partner_customer_usd.id,
            'partner_invoice_id': cls.partner_customer_usd.id,
            'partner_shipping_id': cls.partner_customer_usd.id,
            'pricelist_id': cls.pricelist_usd.id,
        })
        cls.sol_product_order = cls.env['sale.order.line'].create({
            'name': cls.product_order.name,
            'product_id': cls.product_order.id,
            'product_uom_qty': 2,
            'product_uom': cls.product_order.uom_id.id,
            'price_unit': cls.product_order.list_price,
            'order_id': cls.sale_order.id,
            'tax_id': False,
        })
        cls.sol_serv_deliver = cls.env['sale.order.line'].create({
            'name': cls.service_deliver.name,
            'product_id': cls.service_deliver.id,
            'product_uom_qty': 2,
            'product_uom': cls.service_deliver.uom_id.id,
            'price_unit': cls.service_deliver.list_price,
            'order_id': cls.sale_order.id,
            'tax_id': False,
        })
        cls.sol_serv_order = cls.env['sale.order.line'].create({
            'name': cls.service_order.name,
            'product_id': cls.service_order.id,
            'product_uom_qty': 2,
            'product_uom': cls.service_order.uom_id.id,
            'price_unit': cls.service_order.list_price,
            'order_id': cls.sale_order.id,
            'tax_id': False,
        })
        cls.sol_product_deliver = cls.env['sale.order.line'].create({
            'name': cls.product_deliver.name,
            'product_id': cls.product_deliver.id,
            'product_uom_qty': 2,
            'product_uom': cls.product_deliver.uom_id.id,
            'price_unit': cls.product_deliver.list_price,
            'order_id': cls.sale_order.id,
            'tax_id': False,
        })

    def test_sale_order(self):
        """ Test the sales order flow (invoicing and quantity updates)
            - Invoice repeatedly while varrying delivered quantities and check that invoice are always what we expect
        """
        # DBO TODO: validate invoice and register payments
        Invoice = self.env['account.invoice']
        self.sale_order.order_line.read(['name', 'price_unit', 'product_uom_qty', 'price_total'])

        self.assertEqual(self.sale_order.amount_total, sum([2 * p.list_price for p in self.product_map.values()]), 'Sale: total amount is wrong')
        self.sale_order.order_line._compute_product_updatable()
        self.assertTrue(self.sale_order.order_line[0].product_updatable)
        # send quotation
        self.sale_order.force_quotation_send()
        self.assertTrue(self.sale_order.state == 'sent', 'Sale: state after sending is wrong')
        self.sale_order.order_line._compute_product_updatable()
        self.assertTrue(self.sale_order.order_line[0].product_updatable)

        # confirm quotation
        self.sale_order.action_confirm()
        self.assertTrue(self.sale_order.state == 'sale')
        self.assertTrue(self.sale_order.invoice_status == 'to invoice')

        # create invoice: only 'invoice on order' products are invoiced
        inv_id = self.sale_order.action_invoice_create()
        invoice = Invoice.browse(inv_id)
        self.assertEqual(len(invoice.invoice_line_ids), 2, 'Sale: invoice is missing lines')
        self.assertEqual(invoice.amount_total, sum([2 * p.list_price if p.invoice_policy == 'order' else 0 for p in self.product_map.values()]), 'Sale: invoice total amount is wrong')
        self.assertTrue(self.sale_order.invoice_status == 'no', 'Sale: SO status after invoicing should be "nothing to invoice"')
        self.assertTrue(len(self.sale_order.invoice_ids) == 1, 'Sale: invoice is missing')
        self.sale_order.order_line._compute_product_updatable()
        self.assertFalse(self.sale_order.order_line[0].product_updatable)

        # deliver lines except 'time and material' then invoice again
        for line in self.sale_order.order_line:
            line.qty_delivered = 2 if line.product_id.expense_policy == 'no' else 0
        self.assertTrue(self.sale_order.invoice_status == 'to invoice', 'Sale: SO status after delivery should be "to invoice"')
        inv_id = self.sale_order.action_invoice_create()
        invoice2 = Invoice.browse(inv_id)
        self.assertEqual(len(invoice2.invoice_line_ids), 2, 'Sale: second invoice is missing lines')
        self.assertEqual(invoice2.amount_total, sum([2 * p.list_price if p.invoice_policy == 'delivery' else 0 for p in self.product_map.values()]), 'Sale: second invoice total amount is wrong')
        self.assertTrue(self.sale_order.invoice_status == 'invoiced', 'Sale: SO status after invoicing everything should be "invoiced"')
        self.assertTrue(len(self.sale_order.invoice_ids) == 2, 'Sale: invoice is missing')

        # go over the sold quantity
        self.sol_serv_order.write({'qty_delivered': 10})
        self.assertTrue(self.sale_order.invoice_status == 'upselling', 'Sale: SO status after increasing delivered qty higher than ordered qty should be "upselling"')

        # upsell and invoice
        self.sol_serv_order.write({'product_uom_qty': 10})

        inv_id = self.sale_order.action_invoice_create()
        invoice3 = Invoice.browse(inv_id)
        self.assertEqual(len(invoice3.invoice_line_ids), 1, 'Sale: third invoice is missing lines')
        self.assertEqual(invoice3.amount_total, 8 * self.product_map['serv_order'].list_price, 'Sale: second invoice total amount is wrong')
        self.assertTrue(self.sale_order.invoice_status == 'invoiced', 'Sale: SO status after invoicing everything (including the upsel) should be "invoiced"')

    def test_unlink_cancel(self):
        """ Test deleting and cancelling sales orders depending on their state and on the user's rights """
        # SO in state 'draft' can be deleted
        so_copy = self.sale_order.copy()
        with self.assertRaises(AccessError):
            so_copy.sudo(self.user_employee).unlink()
        self.assertTrue(so_copy.sudo(self.user_manager).unlink(), 'Sale: deleting a quotation should be possible')

        # SO in state 'cancel' can be deleted
        so_copy = self.sale_order.copy()
        so_copy.action_confirm()
        self.assertTrue(so_copy.state == 'sale', 'Sale: SO should be in state "sale"')
        so_copy.action_cancel()
        self.assertTrue(so_copy.state == 'cancel', 'Sale: SO should be in state "cancel"')
        with self.assertRaises(AccessError):
            so_copy.sudo(self.user_employee).unlink()
        self.assertTrue(so_copy.sudo(self.user_manager).unlink(), 'Sale: deleting a cancelled SO should be possible')

        # SO in state 'sale' or 'done' cannot be deleted
        self.sale_order.action_confirm()
        self.assertTrue(self.sale_order.state == 'sale', 'Sale: SO should be in state "sale"')
        with self.assertRaises(UserError):
            self.sale_order.sudo(self.user_manager).unlink()

        self.sale_order.action_done()
        self.assertTrue(self.sale_order.state == 'done', 'Sale: SO should be in state "done"')
        with self.assertRaises(UserError):
            self.sale_order.sudo(self.user_manager).unlink()

    def test_cost_invoicing(self):
        """ Test confirming a vendor invoice to reinvoice cost on the so """
        # force the pricelist to have the same currency as the company
        self.pricelist_usd.currency_id = self.env.ref('base.main_company').currency_id

        serv_cost = self.env['product.product'].create({
            'name': "Ordered at cost",
            'standard_price': 160,
            'list_price': 180,
            'type': 'consu',
            'invoice_policy': 'order',
            'expense_policy': 'cost',
            'default_code': 'PROD_COST',
            'service_type': 'manual',
        })
        prod_gap = self.service_order
        so = self.env['sale.order'].create({
            'partner_id': self.partner_customer_usd.id,
            'partner_invoice_id': self.partner_customer_usd.id,
            'partner_shipping_id': self.partner_customer_usd.id,
            'order_line': [(0, 0, {'name': prod_gap.name, 'product_id': prod_gap.id, 'product_uom_qty': 2, 'product_uom': prod_gap.uom_id.id, 'price_unit': prod_gap.list_price})],
            'pricelist_id': self.pricelist_usd.id,
        })
        so.action_confirm()
        so._create_analytic_account()

        company = self.env.ref('base.main_company')
        journal = self.env['account.journal'].create({'name': 'Purchase Journal - Test', 'code': 'STPJ', 'type': 'purchase', 'company_id': company.id})
        invoice_vals = {
            'name': '',
            'type': 'in_invoice',
            'partner_id': self.partner_customer_usd.id,
            'invoice_line_ids': [(0, 0, {'name': serv_cost.name, 'product_id': serv_cost.id, 'quantity': 2, 'uom_id': serv_cost.uom_id.id, 'price_unit': serv_cost.standard_price, 'account_analytic_id': so.analytic_account_id.id, 'account_id': self.account_income.id})],
            'account_id': self.account_payable.id,
            'journal_id': journal.id,
            'currency_id': company.currency_id.id,
        }
        inv = self.env['account.invoice'].create(invoice_vals)
        inv.action_invoice_open()
        sol = so.order_line.filtered(lambda l: l.product_id == serv_cost)
        self.assertTrue(sol, 'Sale: cost invoicing does not add lines when confirming vendor invoice')
        self.assertEquals((sol.price_unit, sol.qty_delivered, sol.product_uom_qty, sol.qty_invoiced), (160, 2, 0, 0), 'Sale: line is wrong after confirming vendor invoice')

    def test_sale_with_taxes(self):
        """ Test SO with taxes applied on its lines and check subtotal applied on its lines and total applied on the SO """
        # Create a tax with price included
        tax_include = self.env['account.tax'].create({
            'name': 'Tax with price include',
            'amount': 10,
            'price_include': True
        })
        # Create a tax with price not included
        tax_exclude = self.env['account.tax'].create({
            'name': 'Tax with no price include',
            'amount': 10,
        })

        # Apply taxes on the sale order lines
        self.sol_product_order.write({'tax_id': [(4, tax_include.id)]})
        self.sol_serv_deliver.write({'tax_id': [(4, tax_include.id)]})
        self.sol_serv_order.write({'tax_id': [(4, tax_exclude.id)]})
        self.sol_product_deliver.write({'tax_id': [(4, tax_exclude.id)]})

        # Trigger onchange to reset discount, unit price, subtotal, ...
        for line in self.sale_order.order_line:
            line.product_id_change()
            line._onchange_discount()

        for line in self.sale_order.order_line:
            if line.tax_id.price_include:
                price = line.price_unit * line.product_uom_qty - line.price_tax
            else:
                price = line.price_unit * line.product_uom_qty

            self.assertEquals(float_compare(line.price_subtotal, price, precision_digits=2), 0)

        self.assertEquals(self.sale_order.amount_total,
                          self.sale_order.amount_untaxed + self.sale_order.amount_tax,
                          'Taxes should be applied')

    def test_so_create_multicompany(self):
        # Preparing test Data
        user_demo = self.env.ref('base.user_demo')
        company_1 = self.env.ref('base.main_company')
        company_2 = self.env['res.company'].create({
            'name': 'company 2',
            'parent_id': company_1.id,
        })
        user_demo.write({
            'groups_id': [(4, self.env.ref('sales_team.group_sale_manager').id, False)],
            'company_ids': [(6, False, [company_1.id])],
            'company_id': company_1.id,
        })

        so_partner = self.env.ref('base.res_partner_2')
        so_partner.write({
            'property_account_position_id': False,
        })

        tax_company_1 = self.env['account.tax'].create({
            'name': 'T1',
            'amount': 90,
            'company_id': company_1.id,
        })

        tax_company_2 = self.env['account.tax'].create({
            'name': 'T2',
            'amount': 90,
            'company_id': company_2.id,
        })

        product_shared = self.env['product.template'].create({
            'name': 'shared product',
            'taxes_id': [(6, False, [tax_company_1.id, tax_company_2.id])],
        })

        # Use case
        so_1 = self.env['sale.order'].sudo(user_demo.id).create({
            'partner_id': so_partner.id,
            'company_id': company_1.id,
        })
        so_1.invalidate_cache()

        # This is what is done when importing the csv lines (on sale.order):
        # id,order_line/product_id
        # __export__.sale_order_37_1bb960ba,Product name
        so_1.write({
            'order_line': [(0, False, {'product_id': product_shared.product_variant_id.id, 'order_id': so_1.id})],
        })

        self.assertEqual(set(so_1.order_line.tax_id.ids), set([tax_company_1.id]),
            'Only taxes from the right company are put by default')


```

![](/lab5/test_result.jpg)